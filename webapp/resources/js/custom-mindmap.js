/**
 * PrimeFaces Custom Mindmap Widget
 */
var imgWidth = 110;
var imgHeight = imgWidth * 1.13;
var circles = [];

function reordenateTags() {
	var svgElment = document.getElementsByTagName('svg')[0];
	var childNode;
	var imageNodes = [];
	var textNodes = [];
	var pathNodes = [];
	var circleNodes = [];
	for(var i = 0; i < svgElment.childNodes.length; i++) {
		childNode = svgElment.childNodes[i];
		if(childNode.tagName == "image") {
			imageNodes.push(childNode);
		}
		if(childNode.tagName == "text") {
			textNodes.push(childNode);
		}
		if(childNode.tagName == "path") {
			pathNodes.push(childNode);
		}
		if(childNode.tagName == "circle") {
			circleNodes.push(childNode);
		}
	}
	svgElment.innerHTML = '';
	for(var i = 0; i < pathNodes.length; i++) {
		svgElment.appendChild(pathNodes[i]); 
	}
	for(var i = 0; i < imageNodes.length; i++) {
		svgElment.appendChild(imageNodes[i]); 
	}
	for(var i = 0; i < textNodes.length; i++) {
		svgElment.appendChild(textNodes[i]); 
	}
	for(var i = 0; i < circleNodes.length; i++) {
		svgElment.appendChild(circleNodes[i]); 
	}
}

function textWrap(text) {
	var label = "";
	var wordsAux = text.split(/\/| /);
	var words = [];
	for(var i = 0; i < wordsAux.length; i++) {
		if(wordsAux[i].length > 0) {
			words.push(wordsAux[i])
		}
	}
	var maxWords = words.length > 4 ? 4 : words.length;
	for(var i = 0; i <= maxWords; i++) {
		if(i == maxWords && words.length > 4) {
			label = label.substring(0, (label.length - 1));
			label = label + "...";
		} else {
			if(i < words.length) {
				label = label + words[i] + " ";
			}
		}
	}
	return label.replace(/ /g, '\n');;
}

PrimeFaces.widget.Mindmap = PrimeFaces.widget.DeferredWidget.extend({
    
    init: function(cfg) {
        this._super(cfg);
        
        this.renderDeferred();
    },
    
    _render: function() {
        this.cfg.width = this.jq.width();
        this.cfg.height = this.jq.height();
        this.cfg.centerX = this.cfg.width / 2;
        this.cfg.centerY = this.cfg.height / 2;
        this.raphael = new Raphael(this.id, this.cfg.width, this.cfg.height);
        this.cfg.centerX = this.cfg.centerX - (imgWidth / 2);
        this.cfg.centerY = this.cfg.centerY - (imgHeight / 2); 
        this.nodes = [];

        if(this.cfg.model) {            
            //root
            this.root = this.createNode((this.cfg.centerX + (imgWidth / 2)), (this.cfg.centerY + (imgHeight / 2)), (imgWidth * 1.35), (imgHeight * 1.35), nodeImg, this.cfg.model, true);
            
            //children
            if(this.cfg.model.children) {
                this.createSubNodes(this.root);
            }
            
            this.tooltip = $('<div style="border-radius: 10px !important; border: 2px solid #666 !important; font-size: 11px !important;" class="ui-tooltip ui-mindmap-tooltip ui-widget ui-widget-content ui-corner-all"></div>').appendTo(document.body);
            reordenateTags();
        }
    },
    
    createNode: function(centerX, centerY, width, height, img, model, isParent) {
    	
    	var node = this.raphael.image(img, (centerX - (width / 2)), (centerY - (height / 2)), width, height)
        .attr({ "clip-rect": (centerX - (width / 2)) + ", " + (centerY - (height / 2)) + ", " + width + ", " + height })
        .data('model', model)
        .data('connections', [])
        .data('widget', this);
                            
        var label = textWrap(model.label);
        
        var fontSize = 14;
        if(isParent) {
        	fontSize = 19;
        }
        
        var text = this.raphael.text(centerX, centerY, label)
        .attr('opacity', 0)
        .attr({'text-align': 'center'})
        .attr({'font-size': fontSize, "font-family": "Arial, Helvetica, sans-serif" })
        .attr({'font-weight': 'bold'})
        .attr({'fill': '#666'});
                
        text.data('node', node);
        node.data('text', text);
        node.data('title', model.label);
        node.mouseover(this.mouseoverNode);
        node.mouseout(this.mouseoutNode);
        text.mouseover(this.mouseoverText);
        text.mouseout(this.mouseoutText);
         
        //node options
        if(model.fill) {
            node.attr({fill: '#' + model.fill});
        }
        
        //show
        node.animate({opacity:1}, this.cfg.effectSpeed);
        text.animate({opacity:1}, this.cfg.effectSpeed);

        //events
        if(model.selectable) {
            node.click(this.clickNode);
            text.click(this.clickNodeText);
            node.attr({cursor:'pointer'});
            text.attr({cursor:'pointer'});
        }
        
        //add to nodes
        this.nodes.push(node);
        
        return node;
    },
    
    mouseoverNode: function() {
        var _self = this.data('widget');
        
        _self.showTooltip(this);
    },
    
    mouseoutNode: function() {
        var _self = this.data('widget');
        
        _self.hideTooltip(this);
    },
    
    mouseoverText: function() {
        var node = this.data('node'),
        _self = node.data('widget');
        
        _self.showTooltip(node);
    },
    
    mouseoutText: function() {
        var node = this.data('node'),
        _self = node.data('widget');
        
        _self.hideTooltip(node);
    },
    
    showTooltip: function(node) {
        var title = node.data('title');
        
        if(title) {
            var _self = node.data('widget'),
            offset = _self.jq.offset();
            var bb = node.getBBox();
            _self.tooltip.text(title)
                        .css(
                            {
                            	'left': offset.left + bb.x + bb.width - 35,
                                'top': offset.top + bb.y,
                                'font-weight': 'bold',
                                'font-family': 'Arial, Helvetica, sans-serif',
                                'z-index': ++PrimeFaces.zindex
                            })
                        .show();
        }
    },
    
    hideTooltip: function(node) {
        var title = node.data('title');
        
        if(title) {
            var _self = node.data('widget');
            
            _self.tooltip.hide();
        }
    },
    
    centerNode: function(node) {
        var _self = this, text;
        model = node.data('model');
        if(!model) {
        	model = this.cfg.model;
        }
        
        this.removeNode(node);
        node = this.createNode((this.cfg.centerX + (imgWidth / 2)), (this.cfg.centerY + (imgHeight / 2)), (imgWidth * 1.35), (imgHeight * 1.35), nodeImg, model, true);
        _self.createSubNodes(node);
        reordenateTags();
        text = node.data('text');
        
        //remove event handlers
        node.unclick(this.clickNode);
        text.unclick(this.clickNodeText);
        node.attr({cursor:'default'});
        text.attr({cursor:'default'});
    },
    
    createSubNodes: function(node) {
        var nodeModel = node.data('model');
        
        if(nodeModel.children) {
            var size = nodeModel.children.length,
            radius = 200,
            capacity = parseInt((radius*2) / 25),
            angleFactor = (360 / Math.min(size, capacity)),
            capacityCounter = 0;
            
            //children
            for(var i = 0 ; i < size; i++) { 
                var childModel = nodeModel.children[i];
                capacityCounter++;

               //coordinates
                var bbox = node.getBBox();
                var angle = ((angleFactor * (i + 1)) / 180) * Math.PI,
                x = (bbox.x + (bbox.width / 2)) - radius * Math.cos(angle),
                y = (bbox.y + (bbox.height / 2)) + radius * Math.sin(angle);
                
                var childNode = this.createNode(x, y, imgWidth, imgHeight, subnodeImg, childModel, false);

                //connection
                var connection = this.raphael.connection(node, childNode, "#000", null, this.cfg.effectSpeed);
                node.data('connections').push(connection);
                childNode.data('connections').push(connection);

                //new ring
                if(capacityCounter === capacity) {
                    radius = radius + 125;
                    capacity = parseInt((radius*2) / 25);
                    angleFactor = (360 / Math.min(capacity, (size - (i + 1) )));
                    capacityCounter = 0;
                }
            }
        }
        
        //parent
        var parentModel = nodeModel.parent;
        if(parentModel) {
            parentModel.selectable = true;
            
            var parentNode = this.createNode(100, 100, imgWidth, imgHeight, nodeImg, parentModel, false);
            parentNode.data('parentNode', true);
            
            //connection
            var parentConnection = this.raphael.connection(node, parentNode, "#000", null, this.cfg.effectSpeed);
            node.data('connections').push(parentConnection);
            parentNode.data('connections').push(parentConnection);
        }
    },

    hasBehavior: function(event) {
        if(this.cfg.behaviors) {
            return this.cfg.behaviors[event] != undefined;
        }
    
        return false;
    },
    
    handleNodeClick: function(node) {
    	
        if(node.dragged) {
            node.dragged = false;
            return;
        }
        
        var _self = this,
        clickTimeout = node.data('clicktimeout');
        
        if(clickTimeout) {
            clearTimeout(clickTimeout);
            node.removeData('clicktimeout');
            
            _self.handleDblclickNode(node);
        }
        else {
            var timeout = setTimeout(function() {
                _self.expandNode(node);
                
                // removes all connection circles.
            	for(var i = 0; i < circles.length; i++) {
            		circles[i].remove();
            	}
                
            }, 300);

            node.data('clicktimeout', timeout);
        }
    },
    
    clickNode: function() {
        var _self = this.data('widget');
        
        _self.handleNodeClick(this);
    },
    
    clickNodeText: function() {
        var node = this.data('node'),
        _self = node.data('widget');
        
        _self.handleNodeClick(node);
    },
    
    handleDblclickNode: function(node) {
        if(this.hasBehavior('dblselect')) {
            var dblselectBehavior = this.cfg.behaviors['dblselect'],
            key = node.data('model').key;

            var ext = {
                params: [
                    {name: this.id + '_nodeKey', value: key}
                ]
            };

            dblselectBehavior.call(this, ext);
        }
    },
    
    expandNode: function(node) {
        var $this = this,
        key = node.data('model').key,
        selectBehavior = this.cfg.behaviors['select'],
        ext = {
            update: this.id,
            params: [
                {name: this.id + '_nodeKey', value: key}
            ],
            onsuccess: function(responseXML, status, xhr) {
                PrimeFaces.ajax.Response.handle(responseXML, status, xhr, {
                        widget: $this,
                        handle: function(content) {
                            var nodeModel = $.parseJSON(content);

                            //update model
                            node.data('model', nodeModel);

                            node.data('connections', []);

                            //remove other nodes
                            for(var j = 0; j < this.nodes.length; j++) {
                                var otherNode = this.nodes[j], nodeKey;
                                if(otherNode.data('model')) {
                                	nodeKey = otherNode.data('model').key;
                                	if(nodeKey !== key) {
                                        this.removeNode(otherNode);
                                    }
                                }
                            }

                            this.nodes = [];
                            this.nodes.push(node);

                            this.centerNode(node);
                        }
                    });

                return true;
            }
        };

        selectBehavior.call(this, ext);      
    },
        
    removeNode: function(node) {
        //test
        node.data('text').remove();
         
        //connections 
        var connections = node.data('connections');
        for(var i = 0; i < connections.length; i++) {
            connections[i].line.remove();
        }
        
        //data
        node.removeData();
        
        //ellipse
        node.animate({opacity:0}, this.cfg.effectSpeed, null, function() {
            this.remove();
        });
    },
    
    updateConnections: function(node) {
        var connections = node.data('connections');
        
        for(var i = 0; i < connections.length; i++) {
            this.raphael.connection(connections[i]);
        }
        
        this.raphael.safari();
    }
});

Raphael.fn.connection = function (obj1, obj2, line, bg, effectSpeed) {
	
	var bb1 = obj1.getBBox();
    var	bb2 = obj2.getBBox();
    
    if(!obj2.data('parentNode')) {
	    var circle = this.circle((((bb1.x + (bb1.width / 2)) + (bb2.x + (bb2.width / 2))) / 2), (((bb1.y + (bb1.height / 2)) + (bb2.y + (bb2.height / 2))) / 2), 10)
	    	.attr({stroke: "#666", fill: "#FFF", "stroke-width": 3}).attr('opacity', 0).animate({opacity:1}, effectSpeed);
	    circles.push(circle);
    }
	
	var pathLine = '' + 
    'M' + (bb1.x + (bb1.width / 2)) + ' ' + (bb1.y + (bb1.height / 2)) + ' ' +
    'L' + (bb2.x + (bb2.width / 2)) + ' ' + (bb2.y + (bb2.height / 2)) + ' ' + 
    'Z';

	var path = this.path(pathLine).attr({stroke: "#666", fill: "none", "stroke-width": 3}).attr('opacity', 0).animate({opacity:1}, effectSpeed);
	
	return {
        bg: '#888',
        line: path,
        from: obj1,
        to: obj2
    };
};