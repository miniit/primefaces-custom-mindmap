# About

Project from Workana which the goal is to develop a custom mindmap component to the Primefaces technology:

http://www.primefaces.org/showcase/ui/data/mindmap.xhtml

# Project

Workana project: https://www.workana.com/job/alterar-layout-default-mindmap-primefaces

# Main developer

@rafaeloragio
